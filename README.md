# DKX/Pagination

Simple pagination

## Installation

```bash
$ dotnet add package DKX.Pagination
```

## Usage

```c#
using DKX.Pagination;

var itemsPerPage = 20;
var paginationBase = 1;

var factory = new PaginatorFactory
{
    ItemsPerPage = itemsPerPage,
    Base = paginationBase,
};

// ... later in the app

var totalCount = 5000;
var currentPage = 3;

var paginator = factory.Create(totalCount, currentPage);
```

## Paginated data

```c#
using DKX.Pagination;

var data = await repository.GetAllUsers();
var paginatedData = new PaginatedData<User>(paginator, data);
```
