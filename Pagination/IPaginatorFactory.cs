namespace DKX.Pagination
{
	public interface IPaginatorFactory
	{
		int ItemsPerPage { get; set; }

		int Base { get; set; }

		IPaginator Create(long totalCount, int? page = null, int? itemsPerPage = null);
	}
}
