using System;

namespace DKX.Pagination
{
	public class Paginator : IPaginator
	{
		private readonly int _page;
		
		public Paginator(
			long totalCount,
			int page = IPaginator.DefaultPage,
			int itemsPerPage = IPaginator.DefaultItemsPerPage,
			int baseNum = IPaginator.DefaultBase
		)
		{
			TotalCount = Math.Max(0, totalCount);
			ItemsPerPage = Math.Max(1, itemsPerPage);
			Base = baseNum;
			_page = page;
		}
		
		public long TotalCount { get; }
		
		public int ItemsPerPage { get; }
		
		public int Base { get; }

		public int Page => Base + PageIndex;

		public int FirstPage => Base;

		public int LastPage => Base + Math.Max(0, PageCount - 1);

		public int PageIndex => Math.Min(Math.Max(0, _page - Base), Math.Max(0, PageCount - 1));

		public int PageCount => (int) Math.Ceiling(TotalCount / (float) ItemsPerPage);

		public long Length => Math.Min(ItemsPerPage, TotalCount - PageIndex * ItemsPerPage);

		public long Offset => PageIndex * ItemsPerPage;

		public long CountdownOffset => Math.Max(0, TotalCount - (PageIndex + 1) * ItemsPerPage);

		public bool IsFirst => PageIndex == 0;

		public bool IsLast => PageIndex >= PageCount - 1;
	}
}
