namespace DKX.Pagination
{
	public class PaginatorFactory : IPaginatorFactory
	{
		public int ItemsPerPage { get; set; } = IPaginator.DefaultItemsPerPage;

		public int Base { get; set; } = IPaginator.DefaultBase;
		
		public IPaginator Create(long totalCount, int? page = null, int? itemsPerPage = null)
		{
			return new Paginator(totalCount, page ?? IPaginator.DefaultPage, itemsPerPage ?? ItemsPerPage, Base);
		}
	}
}
