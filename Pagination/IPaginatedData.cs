using System.Collections.Generic;

namespace DKX.Pagination
{
	public interface IPaginatedData<out TSource>
	{
		IPaginator Paginator { get; }

		IEnumerable<TSource> Data { get; }
	}
}
