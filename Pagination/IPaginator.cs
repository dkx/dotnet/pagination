namespace DKX.Pagination
{
	public interface IPaginator
	{
		public const int DefaultPage = 1;

		public const int DefaultItemsPerPage = 10;

		public const int DefaultBase = 1;

		long TotalCount { get; }

		int Page { get; }

		int ItemsPerPage { get; }

		int Base { get; }

		int FirstPage { get; }

		int LastPage { get; }

		int PageIndex { get; }

		int PageCount { get; }

		long Length { get; }

		long Offset { get; }

		long CountdownOffset { get; }

		bool IsFirst { get; }

		bool IsLast { get; }
	}
}
