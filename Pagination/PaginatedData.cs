using System.Collections.Generic;

namespace DKX.Pagination
{
	public class PaginatedData<TSource> : IPaginatedData<TSource>
	{
		public PaginatedData(IPaginator paginator, IEnumerable<TSource> data)
		{
			Paginator = paginator;
			Data = data;
		}

		public IPaginator Paginator { get; }
		
		public IEnumerable<TSource> Data { get; }
	}
}
