using DKX.Pagination;
using Xunit;

namespace DKX.PaginationTests
{
	public class PaginatorFactoryTest
	{
		[Fact]
		public void Create()
		{
			var factory = new PaginatorFactory
			{
				ItemsPerPage = 7,
				Base = 0,
			};

			var paginator = factory.Create(3, 1);

			Assert.Equal(7, paginator.ItemsPerPage);
			Assert.Equal(0, paginator.Base);
		}
		
		[Fact]
		public void Create_OverrideItemsPerPage()
		{
			var factory = new PaginatorFactory
			{
				ItemsPerPage = 7,
				Base = 0,
			};

			var paginator = factory.Create(3, 1, 9);

			Assert.Equal(9, paginator.ItemsPerPage);
			Assert.Equal(0, paginator.Base);
		}
	}
}
