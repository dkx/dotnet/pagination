using DKX.Pagination;
using Moq;
using Xunit;

namespace DKX.PaginationTests
{
	public class PaginatedDataTest
	{
		[Fact]
		public void Getters()
		{
			var paginator = new Mock<IPaginator>(MockBehavior.Strict);
			var data = new string[] { };

			var paginatedData = new PaginatedData<string>(paginator.Object, data);

			Assert.Same(paginator.Object, paginatedData.Paginator);
			Assert.Same(data, paginatedData.Data);
		}
	}
}
