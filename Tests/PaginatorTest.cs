using DKX.Pagination;
using Xunit;

namespace DKX.PaginationTests
{
	public class PaginatorTest
	{
		[Fact]
		public void Test01()
		{
			var paginator = new Paginator(7, 3, 6, 0);

			Assert.Equal(1, paginator.Page);
			Assert.Equal(2, paginator.PageCount);
			Assert.Equal(0, paginator.FirstPage);
			Assert.Equal(1, paginator.LastPage);
			Assert.Equal(6, paginator.Offset);
			Assert.Equal(0, paginator.CountdownOffset);
			Assert.Equal(1, paginator.Length);
		}
		
		[Fact]
		public void Test02()
		{
			var paginator = new Paginator(7, -1, 6, 0);

			Assert.Equal(0, paginator.Page);
			Assert.Equal(0, paginator.Offset);
			Assert.Equal(1, paginator.CountdownOffset);
			Assert.Equal(6, paginator.Length);
		}
		
		[Fact]
		public void Test03()
		{
			var paginator = new Paginator(7, -1, 7, 0);

			Assert.Equal(0, paginator.Page);
			Assert.Equal(1, paginator.PageCount);
			Assert.Equal(0, paginator.FirstPage);
			Assert.Equal(0, paginator.LastPage);
			Assert.Equal(0, paginator.Offset);
			Assert.Equal(0, paginator.CountdownOffset);
			Assert.Equal(7, paginator.Length);
		}
		
		[Fact]
		public void Test04()
		{
			var paginator = new Paginator(-1, -1, 7, 0);

			Assert.Equal(0, paginator.Page);
			Assert.Equal(0, paginator.PageCount);
			Assert.Equal(0, paginator.FirstPage);
			Assert.Equal(0, paginator.LastPage);
			Assert.Equal(0, paginator.Offset);
			Assert.Equal(0, paginator.CountdownOffset);
			Assert.Equal(0, paginator.Length);
		}
		
		[Fact]
		public void Test05()
		{
			var paginator = new Paginator(7, 3, 6, 1);

			Assert.Equal(2, paginator.Page);
			Assert.Equal(2, paginator.PageCount);
			Assert.Equal(1, paginator.FirstPage);
			Assert.Equal(2, paginator.LastPage);
			Assert.Equal(6, paginator.Offset);
			Assert.Equal(0, paginator.CountdownOffset);
			Assert.Equal(1, paginator.Length);
		}
		
		[Fact]
		public void Test06()
		{
			var paginator = new Paginator(0, 1, 1);
			
			Assert.True(paginator.IsFirst);
			Assert.True(paginator.IsLast);
		}
		
		[Fact]
		public void Test07()
		{
			var paginator = new Paginator(1, 1, 1);
			
			Assert.True(paginator.IsFirst);
			Assert.True(paginator.IsLast);
		}
		
		[Fact]
		public void Test08()
		{
			var paginator = new Paginator(2, 1, 1);
			
			Assert.True(paginator.IsFirst);
			Assert.False(paginator.IsLast);
		}
		
		[Fact]
		public void Test09()
		{
			var paginator = new Paginator(2, 2, 1);
			
			Assert.False(paginator.IsFirst);
			Assert.True(paginator.IsLast);
		}
	}
}
