.PHONY: deps-install
deps-install:
	dotnet restore

.PHONY: test
test:
	dotnet test Tests/

.PHONY: tag
tag:
	sed -i "s/0.0.0-version-placeholder/$(version)/" Pagination/Pagination.csproj

.PHONY: build
build:
	dotnet build

.PHONY: publish
publish:
	dotnet nuget push ./Pagination/bin/Debug/DKX.Pagination.$(version).nupkg -k $(api_key) -s https://api.nuget.org/v3/index.json
